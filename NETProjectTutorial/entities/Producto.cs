﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Producto
    {
        private int id;
        private string sku;
        private string nombre;
        private string descripcion;
        private int cantidad;
        private double precio;


        public Producto(int id, string sku, string nombre, string descripcion, int cantidad, double precio)
        {
            this.id = id;
            this.sku = sku;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.cantidad = cantidad;
            this.precio = precio;
        }

        public int Id { get => id; set => id = value; }
        public string Sku { get => sku; set => sku = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double Precio { get => precio; set => precio = value; }

        public override string ToString()
        {
            return Sku + " " + Nombre ;
        }
    }
}

