﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Factura
    {
        private int id;
        private string cod_factura;
        private DateTime fecha;
        private Empleado empleado;
        private string observaciones;
        private double subtotal;
        private double iva;
        private double total;

        public Factura(int id, string cod_factura, DateTime fecha, Empleado empleado, string observaciones, double subtotal, double iva, double total)
        {
            this.id = id;
            this.cod_factura = cod_factura;
            this.fecha = fecha;
            this.empleado = empleado;
            this.observaciones = observaciones;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
        }

        public int Id { get => id; set => id = value; }
        public string Cod_factura { get => cod_factura; set => cod_factura = value; }
        public DateTime Fecha { get => fecha; set => fecha = value; }
        public string Observaciones { get => observaciones; set => observaciones = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }
        public double Iva { get => iva; set => iva = value; }
        public double Total { get => total; set => total = value; }
        internal Empleado Empleado { get => empleado; set => empleado = value; }
    }
}
