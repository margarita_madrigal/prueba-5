﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Empleado
    {
        private int id;
        private string nombre;
        private string apellidos;
        private string cedula;
        private string inss;
        private string direccion;
        private double salario;
        private string tconvencional;
        private string tcelular;
        private SEXO sexo;

        public Empleado(int id, string nombre, string apellidos, string cedula, string inss, string direccion, double salario, string tconvencional, string tcelular, SEXO sexo)
        {
            this.id = id;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.cedula = cedula;
            this.inss = inss;
            this.direccion = direccion;
            this.salario = salario;
            this.tconvencional = tconvencional;
            this.tcelular = tcelular;
            this.sexo = sexo;
        }

        public enum SEXO
        {
            FEMALE, MALE
        }


        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Cedula { get => cedula; set => cedula = value; }
        public string Inss { get => inss; set => inss = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public double Salario { get => salario; set => salario = value; }
        public string Tconvencional { get => tconvencional; set => tconvencional = value; }
        public string Tcelular { get => tcelular; set => tcelular = value; }
        internal SEXO Sexo { get => sexo; set => sexo = value; }

        public override string ToString()
        {
            return Cedula + " " + Nombre + " " + Apellidos;
        }



    }
}
