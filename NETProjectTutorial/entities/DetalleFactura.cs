﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class DetalleFactura
    {
        private int id;
        private Factura factura;
        private Producto producto;
        private int cantidad;
        private double precio;

        public DetalleFactura(int id, Factura factura, Producto producto, int cantidad, double precio)
        {
            this.Id = id;
            this.Factura = factura;
            this.Producto = producto;
            this.Cantidad = cantidad;
            this.Precio = precio;
        }

        public int Id { get => id; set => id = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double Precio { get => precio; set => precio = value; }
        internal Factura Factura { get => factura; set => factura = value; }
        internal Producto Producto { get => producto; set => producto = value; }


    }
}
